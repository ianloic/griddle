int w = 10;
int h = 20;
int pw = 60;
int ph = 20;

int i = 0;

color palette[][];

class Leds {
}

Leds leds;

void setup() {
  size(w*pw, h*ph);
  noStroke();
  
  leds = new Leds();
}

void draw() {
  background(0);
  for (int y=0; y<h; y++) {
    for (int x=0; x<w; x++) {
      int color_width = 50;
      float X = ((float)x)/w - 0.5;
      float Y = ((float)y)/h - 0.5;
      float time = millis()/1000.0;
      float v1 = sin(Y*10 + time);
      float v2 = sin(10*(X*sin(time/2)+Y*cos(time/3))+time);
      
      float cx = X + 0.5 * sin(time/5);
      float cy = Y + 0.5 * sin(time/3);
      float v3 = sin(sqrt(100*(cx*cx+cy*cy)+1) + time);
      
      //color c = makeColor(255, 50, (int)(100*v)+50);
      //float v = v1*v2*v3;
      float v = v1+v2+v3;
      color c = makeColor((int)(color_width*v+time*10), (int)(20*v+80), 50);
      fill(c);
      rect(x*pw,y*ph,pw,ph);
    }
  }
  
  i = (i+1) % 10;
  
  _delay(100);
}

void _delay(int millis) {
  try {
    Thread.sleep(millis);
  } catch (Exception e) { }
}
